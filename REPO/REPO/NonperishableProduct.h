#pragma once
#include <cstdint>
#include <string>
#include "IPriceable.h"
#include "Product.h"
#include "NonperishableProductType.h"

class NonperishableProduct : public Product
{
public:
	NonperishableProduct(const int32_t id, const std::string& name, const float rawPrice, NonperishableProductType type);
	NonperishableProductType GetType() const;
	uint8_t GetVAT() const override;
	float GetPrice() const override;//alt+shift 

private:
	NonperishableProductType m_type;
	static const uint8_t m_VAT = 19;
};

